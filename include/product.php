<?php

Class mercosistem_product extends merco {

  public function __construct() {
          parent::__construct();
  }

  public function list_product($sku) {

    $params = array('usuario' => USER,
                        "senha" => PASSWORD,
                        "campo" => "CodFab",
                        "filtro" => $sku
                        );
      $return = $this->soap_client->ProdutoPorFiltroDisponiveis($params);
      $return = $this->normalize_response();

      return $return;
  }

  public function get_product_code($sku){
    $params = array('usuario' => USER,
                        "senha" => PASSWORD,
                        "campo" => "CodFab",
                        "filtro" => $sku
                        );

    $soap_prod = $this->soap_client->ProdutoResumidoPorFiltroDisponiveis($params);
    if(!isset($soap_prod->ProdutoResumidoPorFiltroDisponiveisResult->ProdutoResumido)) return false;

    $codigo_prod_merco = $soap_prod->ProdutoResumidoPorFiltroDisponiveisResult->ProdutoResumido->Codigo;

    return $codigo_prod_merco;
  }

  public function get_product_qty($sku){
    $params_qte = array('usuario' => USER,
                        "senha" => PASSWORD,
                        "codProduto" => $this->get_product_code($sku)
                        );

    $soap_prod_qtd = $this->soap_client->QtdeEstoqueProduto($params_qte)->QtdeEstoqueProdutoResult->QtdeAtual;

    return $soap_prod_qtd;
  }

  public function get_prices(){
    $params_list = array('usuario' => USER,
                        "senha" => PASSWORD
                        );

    $soap_prod_list = $this->soap_client->RetornaLstpreco($params_list)->RetornaLstprecoResult->Lstpreco;

    return $soap_prod_list;
  }

  public function get_product_prices($sku){
    $cod = $this->get_product_code($sku);

    $soap_list = $this->get_prices();

    foreach ($soap_list as $key => $value) {
      $descricao_list = $value->_descricao;

      if($descricao_list == "SITE") {
        $list = $value->_ListaProdutoList->ListaProduto;
        break;
      }
    }

    foreach ($list as $key => $value) {
      $product_id = (int)$value->_cod_prod;
      // $id = (int)$id;

      if($product_id == $cod) {
        $return = [];
        if(!empty($value->_vlr_1)) {
          $return['preco1'] = (float)$value->_vlr_1;
        } else {
          $return['preco1'] = (float)0;
        }

        if(!empty($value->_vlr_2)) {
          $return['preco2'] = (float)$value->_vlr_2;
        } else {
          $return['preco2'] = (float)0;
        }

        if(!empty($value->_vlr_3)) {
          $return['preco3'] = (float)$value->_vlr_3;
        } else {
          $return['preco3'] = (float)0;
        }

        if(!empty($value->_vlr_4)) {
          $return['preco4'] = (float)$value->_vlr_4;
        } else {
          $return['preco4'] = (float)0;
        }

        if(!empty($value->_vlr_5)) {
          $return['preco5'] = (float)$value->_vlr_5;
        } else {
          $return['preco5'] = (float)0;
        }

        if(!empty($value->_vlr_base)) {
          $return['valor_base'] = (float)$value->_vlr_base;
        } else {
            $return['valor_base'] = (float)0;
        }

        return $return;
        break;
      }
    }
    $return['preco1'] = (float)0;
    $return['preco2'] = (float)0;
    $return['preco3'] = (float)0;
    $return['preco4'] = (float)0;
    $return['preco5'] = (float)0;
    $return['valor_base'] = (float)0;

    return $return;

  }
}
 ?>
