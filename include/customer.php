<?php

Class mercosistem_customer extends merco {

  public function __construct() {
          parent::__construct();
  }

//VER COM O YURI SOBRE OS CAMPOS PREENCHIDOS MANUALMENTE
  public function put_register_customer($customerData){

    $data['usuario'] = USER;
    $data['senha'] = PASSWORD;
    $data['tipo_pessoa'] = $customerData->tipo_pessoa;
    $data['razao'] = $this->normalize_data($customerData->razao);
    $data['fantasia'] = $this->normalize_data($customerData->fantasia);
    $data['cpf_cnpj'] = $customerData->cpf_cnpj;
    $data['rg'] = $customerData->rg;
    $data['tipo_endereco_princ'] = $this->normalize_data($customerData->tipo_endereco_princ);
    $data['endereco_princ'] = $this->normalize_data($customerData->endereco_princ);
    $data['num_endereco_princ'] = $customerData->num_endereco_princ;
    $data['cep_endereco_princ'] = $customerData->cep_endereco_princ;
    $data['bairro_endereco_princ'] = $this->normalize_data($customerData->bairro_endereco_princ);
    $data['cidade'] = $this->normalize_data($customerData->cidade);
    $data['estado'] = $this->normalize_data($customerData->estado);
    $data['e_mail'] = $this->normalize_data($customerData->e_mail);
    $data['nome_contato'] = $this->normalize_data($customerData->nome_contato);
    $data['site'] = $this->normalize_data($customerData->site);
    $data['tipo_end_cob'] = $this->normalize_data($customerData->tipo_end_cob);
    $data['end_cob'] = $this->normalize_data($customerData->end_cob);
    $data['num_end_cob'] = $customerData->num_end_cob;
    $data['cep_cob'] = $customerData->cep_cob;
    $data['bairro_cob'] = $this->normalize_data($customerData->bairro_cob);
    $data['cidade_cob'] = $this->normalize_data($customerData->cidade_cob);
    $data['estado_cob'] = $this->normalize_data($customerData->estado_cob);
    $data['tipo_end_entr'] = $this->normalize_data($customerData->tipo_end_entr);
    $data['end_entr'] = $this->normalize_data($customerData->end_entr);
    $data['num_end_entr'] = $customerData->num_end_entr;
    $data['cep_entr'] = $customerData->cep_entr;
    $data['bairro_entr'] = $this->normalize_data($customerData->bairro_entr);
    $data['cidade_entr'] = $this->normalize_data($customerData->cidade_entr);
    $data['estado_entr'] = $this->normalize_data($customerData->estado_entr);
    $data['data_nasc'] = $customerData->data_nasc;
    // $data['insc_estadual'] = $customerData->insc_estadual;
    $data['sexo'] = $customerData->sexo;
    // $data['estado_civil'] = $customerData->estado_civil;
    // $data['insc_munincipal'] = $customerData->insc_munincipal;
    $data['data_cadastro'] = $customerData->data_cadastro;
    $data['codigo_cli_orig'] = $customerData->codigo_cli_orig;
    $data['ddd_fixo'] = $customerData->ddd_fixo;
    $data['fone_fixo'] = $customerData->fone_fixo;
    $data['ddd_cel'] = $customerData->ddd_cel;
    $data['fone_cel'] = $customerData->fone_cel;
    $data['cfop_padrao'] = $customerData->cfop_padrao;
    $data['consumidor_final'] = $customerData->consumidor_final;
    // $data['limite_credito'] = $customerData->limite_credito;
    $data['cod_equipe'] = $customerData->cod_equipe;
    $data['compl_end'] = $this->normalize_data($customerData->compl_end);
    $data['compl_cob'] = $this->normalize_data($customerData->compl_cob);
    $data['compl_entr'] = $this->normalize_data($customerData->compl_entr);

    $data['tipo_frete'] = $customerData->tipo_frete;
    $data['cod_lstpreco'] = $customerData->cod_lstpreco;
    $data['cod_forma_pagto'] = $customerData->cod_forma_pagto;
    $data['prazo_medio_max_venda'] = $customerData->prazo_medio_max_venda;

    // var_dump($data);        //DEBUG
    // exit("New Customer");   //DEBUG

    $return = $this->soap_client->CadastraCliente($data);
    $return = $this->normalize_response();
    // var_dump($return);        //DEBUG
    // exit("New Customer");   //DEBUG
    if(!isset($return->sBody->CadastraClienteResponse->CadastraClienteResult)) {
      var_dump($return->sBody->sFault->faultstring);
      return false;
    }

    return $return->sBody->CadastraClienteResponse->CadastraClienteResult;
  }

  public function get_customer_id($doc){
    if(strlen($doc) == 14) $tipo = 1;
    else $tipo = 0;
    $params = array('usuario' => USER,
                    'senha' => PASSWORD,
                    'tipo_pessoa' => $tipo,
                    'cpf_cnpj' => $doc);

    $return = $this->soap_client->ClientesPorDocumentoDisponiveis($params);

    $return = $this->normalize_response();

    if(!isset($return->sBody->ClientesPorDocumentoDisponiveisResponse->ClientesPorDocumentoDisponiveisResult->aPessoas)) return false;
    return $return->sBody->ClientesPorDocumentoDisponiveisResponse->ClientesPorDocumentoDisponiveisResult->aPessoas->a_EmailList->aEmail->a_cod_pessoa;
  }

  public function insert_customer($customer_data) {
    if($customer_data->cpf_cnpj == '') $customer_data->cpf_cnpj = "00000000000";

    if($customer_data->cpf_cnpj < 10000) {
      $new_customer = $this->put_register_customer($customer_data);
      if(!$new_customer) return false;
      return $new_customer;
    } else {
      $customer_id = $this->get_customer_id($customer_data->cpf_cnpj);

      if(!$customer_id) {
        $new_customer = $this->put_register_customer($customer_data);
        if(!$new_customer) return false;
        return $new_customer;
      }
    }

    return $customer_id;
  }
}
 ?>
