<?php

define("URL",$wsdl_merco);
define("USER",$merco_soap_user);
define("PASSWORD",$merco_soap_password);
define("TEAM_CODE",$team_code);

define("CONSUMIDOR_FINAL",$consumidor_final);
define("COD_LSTPRECO",$cod_lstpreco);
define("COD_FORMA_PAGTO_SITE",$cod_forma_pagto_site);
define("COD_FORMA_PAGTO_MLB",$cod_forma_pagto_mlb);
define("COD_FORMA_PAGTO_B2W",$cod_forma_pagto_b2w);
define("PRAZO_MEDIO_MAX_VENDA",$prazo_medio_max_venda);
//InserirPedido
define("CODLISTAPRECO",$CodListaPreco);
define("CODTRANSPORTADORA",$CodTransportadora);
define("NUMSERIE",$NumSerie);
define("STATUS",$Status);
define("TIPOFRETE",$TipoFrete);
define("TIPOPEDIDO",$TipoPedido);
define("CODCONDICAOPAGTO",$CodCondicaoPagto);
define("CODCONDICAOPAGTOMLB",$CodFormaPagtoMlb);
define("CODCONDICAOPAGTOB2W",$CodFormaPagtoB2w);
 ?>
