<?php
if (!isset($prefix_merco)) $prefix_merco = '';

require_once $prefix_merco.'include/config.php';  //Fixed data (users, passwds, urls)
require_once $prefix_merco.'include/defines.php';   //Object, info etc etc
require_once $prefix_merco.'include/merco_soap.php';   //Merco
require_once $prefix_merco.'include/product.php';   //Merco
require_once $prefix_merco.'include/customer.php';   //Merco
require_once $prefix_merco.'include/order.php';   //Merco
?>
