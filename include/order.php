<?php

Class mercosistem_order extends merco {

  public function __construct() {
    parent::__construct();
    $this->class_products = new mercosistem_product;
  }

  // VERIFICAR COM O YURI OS DADOS PADROES
  public function create_order($orderData) {
      $params = array(
          'usuario' => USER,
          'senha' => PASSWORD,
          'pedidos' => array(
            'PedidoVenda' => array(
              'Cabecalho' => array(
                'CodCliOrig' => $orderData->CodCliOrig,
                'CodEquipe' => $orderData->CodEquipe,
                'CodListaPreco' => $orderData->CodListaPreco,
                'CodTransportadora' => $orderData->CodTransportadora,
                'CpfCnpj' => $orderData->CpfCnpj,
                'DataEmissao' => $orderData->DataEmissao,
                'DataEntrega' => $orderData->DataEntrega,
                'DescontoPercentual' => $orderData->DescontoPercentual,
                'DestBairroEntr' => $this->normalize_data($orderData->DestBairroEntr),
                'DestBairroPrinc' => $this->normalize_data($orderData->DestBairroPrinc),
                'DestCel' => $orderData->DestCel,
                'DestCepEntr' => $orderData->DestCepEntr,
                'DestCepPrinc' => $orderData->DestCepPrinc,
                'DestCidadeEntr' => $this->normalize_data($orderData->DestCidadeEntr),
                'DestCidadePrinc' => $this->normalize_data($orderData->DestCidadePrinc),
                'DestComplementoEntr' => $this->normalize_data($orderData->DestComplementoEntr),
                'DestComplementoPrinc' => $this->normalize_data($orderData->DestComplementoPrinc),
                'DestContato' => $this->normalize_data($orderData->DestContato),
                'DestDataCadastro' => $orderData->DestDataCadastro,
                'DestDataNasc' => $orderData->DestDataNasc,
                'DestDddCel' => $orderData->DestDddCel,
                'DestDddFone' => $orderData->DestDddFone,
                'DestEmail' => $this->normalize_data($orderData->DestEmail),
                'DestEnderecoEntr' => $this->normalize_data($orderData->DestEnderecoEntr),
                'DestEnderecoPrinc' => $this->normalize_data($orderData->DestEnderecoPrinc),
                'DestEstadoEntr' => $this->normalize_data($orderData->DestEstadoEntr),
                'DestEstadoPrinc' => $this->normalize_data($orderData->DestEstadoPrinc),
                // 'DestFantasia' => $orderData->DestFantasia,
                'DestFone' => $orderData->DestFone,
                // 'DestInscEstadual' => $orderData->DestInscEstadual,
                // 'DestInscMunicipal' => $orderData->DestInscMunicipal,
                'DestNumEndEntr' => $orderData->DestNumEndEntr,
                'DestNumEndPrinc' => $orderData->DestNumEndPrinc,
                'DestRazao' => $this->normalize_data($orderData->DestRazao),
                'DestRg' => $orderData->DestRg,
                'DestSexo' => $orderData->DestSexo,
                'DestTipoEndEntr' => $this->normalize_data($orderData->DestTipoEndEntr),
                'DestTipoEndPrinc' => $this->normalize_data($orderData->DestTipoEndPrinc),
                'DestTipoPessoa' => $orderData->DestTipoPessoa,
                'NumPedido' => $orderData->NumPedido,
                'NumSerie' => $orderData->NumSerie,
                'Obs' => $orderData->Obs,
                'Status' => $orderData->Status,
                'TipoFrete' => $orderData->TipoFrete,
                'TipoPedido' => $orderData->TipoPedido,
                'ValorDesconto' => $orderData->ValorDesconto,
                'ValorFrete' => $orderData->ValorFrete,
                'ValorPedido' => $orderData->ValorPedido,
                'ValorProdutos' => $orderData->ValorProdutos
              ),
              'Item' => $orderData->ItemPedido,
              'Pagamento' => array(
                'CodCondicaoPagto' => $orderData->CodCondicaoPagto,
                'CodFormaPagto' => $orderData->CodFormaPagto,
                'Forma' => $this->normalize_data($orderData->Forma),
                'QtdeParcelas' => $orderData->QtdeParcelas,
                'Valor' => $orderData->Valor
              )
            )
          )
        );
        // var_dump($params);        //DEBUG
        // exit("Param Pedido");     //DEBUG
        $return = $this->soap_client->InserirPedido($params);
        // var_dump($return);        //DEBUG
        // exit("Param Pedido");     //DEBUG
        if(isset($return->faultstring)) {
          echo $return->faultstring;
          return false;
        }
        $return = $this->normalize_response();

        return $return;
      }

      public function get_order($order_id) {
        $params = array('usuario' => USER,
        'senha' => PASSWORD,
        'codPedidoIntegracao' => $order_id);

      $return = $this->soap_client->RetornaPedidoPorCodigoIntegracao($params);

      if(isset($return->faultstring)) return $return->faultstring;
        $return = $this->normalize_response();

        if(!isset($return->sBody->RetornaPedidoPorCodigoIntegracaoResponse->RetornaPedidoPorCodigoIntegracaoResult->aPedidoVendaResumido)) return false;

        return $return->sBody->RetornaPedidoPorCodigoIntegracaoResponse->RetornaPedidoPorCodigoIntegracaoResult->aPedidoVendaResumido;
      }
    }
    ?>
