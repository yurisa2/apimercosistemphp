<?php

Class merco {

  public function __construct() {

    $this->soap_client = new SoapClient(URL, array(
      'trace' => 1,
      "exceptions" => 0,
      'style'=> SOAP_DOCUMENT,
      'use'=> SOAP_LITERAL));
    }

    public function normalize_response() {

      $last_response = $this->soap_client->__getLastResponse(); // essa linha pega a resposta CRUA - RAW
      $response = preg_replace("/(<\/?)(\w+):([^>]*>)/", "$1$2$3", str_replace('&#x0;', '', $last_response)); // essa linha transforma o xml-soap em xml1.1 normal e tira o caractere com pau '&#x0;'
      $return = new SimpleXMLElement($response);  // essa linha cria um objeto xml que é esse que vc vai mexer... MANO, TEM CAMPO PRA CARALHO

      return $return;
    }

    public function normalize_words($string) {
      return preg_replace(array("/(á|à|ã|â|ä)/","/(Á|À|Ã|Â|Ä)/","/(é|è|ê|ë)/","/(É|È|Ê|Ë)/","/(í|ì|î|ï)/","/(Í|Ì|Î|Ï)/","/(ó|ò|õ|ô|ö)/","/(Ó|Ò|Õ|Ô|Ö)/","/(ú|ù|û|ü)/","/(Ú|Ù|Û|Ü)/","/(ñ)/","/(Ñ)/","/(ç)/","/(Ç)/"),explode(" ","a A e E i I o O u U n N c C"),$string);
    }

    public function normalize_data($data) {
        $normalized_data = trim(strtoupper($this->normalize_words($data)));

        return $normalized_data;
    }

    public function changestate($state){
      $str = $state;
      $str = str_replace(' ', '', $str);
      $str = preg_replace(array("/(á|à|ã|â|ä)/","/(Á|À|Ã|Â|Ä)/","/(é|è|ê|ë)/","/(É|È|Ê|Ë)/","/(í|ì|î|ï)/","/(Í|Ì|Î|Ï)/","/(ó|ò|õ|ô|ö)/","/(Ó|Ò|Õ|Ô|Ö)/","/(ú|ù|û|ü)/","/(Ú|Ù|Û|Ü)/","/(ñ)/","/(Ñ)/"),explode(" ","a A e E i I o O u U n N"),$str);
      $clean = iconv('UTF-8', 'ASCII//TRANSLIT', $str);
      $no_special_char = preg_replace('/[^A-Za-z0-9\-]/', '', $clean);
      $just_lower = strtolower($no_special_char);
      $state = preg_replace('/[ -]+/' , '-' , $just_lower);

      switch ($state) {
        case "acre":
        return "AC";
        break;
        case "alagoas":
        return "AL";
        break;
        case "amapa":
        return "AP";
        break;
        case "amazonas":
        return "AM";
        break;
        case "bahia":
        return "BA";
        break;
        case "ceara":
        return "CE";
        break;
        case "distritofederal":
        return "DF";
        break;
        case "espiritosanto":
        return "ES";
        break;
        case "goias":
        return "GO";
        break;
        case "maranhao":
        return "MA";
        break;
        case "matogrosso":
        return "MT";
        break;
        case "matogrossodosul":
        return "MS";
        break;
        case "minasgerais":
        return "MG";
        break;
        case "para":
        return "PA";
        break;
        case "paraiba":
        return "PB";
        break;
        case "parana":
        return "PR";
        break;
        case "pernambuco":
        return "PE";
        break;
        case "piaui":
        return "PI";
        break;
        case "riodejaneiro":
        return "RJ";
        break;
        case "riograndedonorte":
        return "RN";
        break;
        case "riograndedosul":
        return "RS";
        break;
        case "rondonia":
        return "RO";
        break;
        case "roraima":
        return "RR";
        break;
        case "santacatarina":
        return "SC";
        break;
        case "saopaulo":
        return "SP";
        break;
        case "sergipe":
        return "SE";
        break;
        case "tocantins":
        return "TO";
        break;
        default:
        return "SP";
      }
    }
  }

  ?>
